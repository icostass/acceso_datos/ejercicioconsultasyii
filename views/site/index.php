<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Consultas de Selección 1';
?>
<div class="site-index">
    
    <div class="jumbotron text-center bg-transparent">
        <hl class="display-4">Consultas de Selección</h1>
    </div>
    
    <div class="body-content">
        <!-- Inicio de la primera fila -->
        <div class="row">
            <!--
            botón de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="card alturaminima">
                    <div class="card-body tarjeta">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                            <?= Html::a('DAO',['site/consulta1'], ['class' => 'btn btn-warning'])?>
                        </p>
                    </div>
                </div>
            </div>
            <!-- 
              Fin botón de consulta 1 
            -->
                 <div class="col-sm-6 col-md-4"> 
                    <div class="card alturaminima">
                         <div class="card-body tarjeta">
                            <h3>Consulta 2</h3>
                            <p>Listar las edades de los cilistas de Artiach</p>
                            <p>
                                <?= Html::a('Active Record',['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                                <?= Html::a('DAO',['site/consulta1'], ['class' => 'btn btn-warning'])?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4"> 
                    <div class="card alturaminima">
                         <div class="card-body tarjeta">
                            <h3>Consulta 3</h3>
                            <p>Listar las edades de los cilistas de Artiach o de Amore Vita</p>
                            <p>
                                <?= Html::a('Active Record',['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                                <?= Html::a('DAO',['site/consulta1'], ['class' => 'btn btn-warning'])?>
                            </p>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row">
                <div class="col-sm-6 col-md-4"> 
                    <div class="card alturaminima">
                         <div class="card-body tarjeta">
                            <h3>Consulta 4</h3>
                            <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                            <p>
                                <?= Html::a('Active Record',['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                                <?= Html::a('DAO',['site/consulta1'], ['class' => 'btn btn-warning'])?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4"> 
                    <div class="card alturaminima">
                         <div class="card-body tarjeta">
                            <h3>Consulta 5</h3>
                            <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                            <p>
                                <?= Html::a('Active Record',['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                                <?= Html::a('DAO',['site/consulta1'], ['class' => 'btn btn-warning'])?>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-4"> 
                    <div class="card alturaminima">
                         <div class="card-body tarjeta">
                            <h3>Consulta 6</h3>
                            <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                            <p>
                                <?= Html::a('Active Record',['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                                <?= Html::a('DAO',['site/consulta1'], ['class' => 'btn btn-warning'])?>
                            </p>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</div>




   